/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wariya.agentvalorant;

/**
 *
 * @author os
 */
public class Controller extends Agent {
    public Controller (String name, String role, String skill1,String skill2,String skill3, String ultimate ){
        super(name,role,skill1,skill2,skill3,ultimate);
    }
    
    @Override
    public void agentspeak(){
        System.out.println("I'm Controller");
    }
    
    @Override 
    public String toString(){
        return "Name: "+name+"\nSkill 1: "+skill1+"\nSkill 2: "
                +skill2+"\nSkill 3: "+skill3+"\nUltimate: "+ultimate;
    }
}
