/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wariya.agentvalorant;

/**
 *
 * @author os
 */
public class TestAgent {
    public static void main(String[] args) {
        Agent agent = new Agent ("A","B","C","D","E","F");
        agent.agentspeak();
        
        Controller viper = new Controller("Viper","Controller","Snake Bite",
        "Poison Cloud","Toxin Screen","Viper's Pit");
        viper.agentspeak();
        System.out.println(viper);
        
        Duelist phoenix = new Duelist("Phoenix","Duelist","Blaze",
        "Curveball","Hot hands","Run it back");
        phoenix.agentspeak();
        System.out.println(phoenix);
        
        Duelist jett = new Duelist("Jett","Duelist","Cloudburst",
        "Updraft","Tailwind","Blade storm");
        jett.agentspeak();
        System.out.println(jett);
        
        Sentinel cypher = new Sentinel("Cypher","Sentinel","Trapwire",
        "Cyber cage","Spycam","Neural theft");
        cypher.agentspeak();
        System.out.println(cypher);
        
        Intiator sova = new Intiator("Sova","Intiator","Owl drone",
        "Shock bolt","Recon bolt","Hunter's fury");
        sova.agentspeak();
        System.out.println(sova);
    }
}
