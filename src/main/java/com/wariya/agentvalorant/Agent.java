/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wariya.agentvalorant;

/**
 *
 * @author os
 */
public class Agent {
    protected String name;
    protected String role;
    protected String skill1;
    protected String skill2;
    protected String skill3;
    protected String ultimate;
    protected Agent(String name, String role, String skill1,String skill2,String skill3, String ultimate ){
        this.name = name;
        this.role = role;
        this.skill1 = skill1;
        this.skill2 = skill2;
        this.skill3 = skill3;
        this.ultimate = ultimate;
    }
    
    public void agentspeak(){
        System.out.println("I'm the new Agent");
    }
    public String getName(){
        return name;
    }
    public String getRole(){
        return role;
    }
    public String getSkill1(){
        return skill1;
    }
    public String getSkill2(){
        return skill2;
    }
    public String getSkill3(){
        return skill3;
    }
    public String getUltimate(){
        return ultimate;
    }
}
